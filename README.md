# get-residue-energies

### Introduction

namdEnergy plugin of VMD is a script which can be used for calculating the total energy of a selection of amino acid residues or the interaction energies between selected pairs of amino acids from trajectories of protein molecular dynamics simulations. In its current implementation, namdEnergy is single-threaded and must be used in a tcl script if automatic calculation of a lot of energetical interactions is required. I wrote a python script (getResidueEnergies.py) which basically wraps the namdEnergy plugin and utilizes additional cores for faster calculation of (interaction) energies between two selected amino acid groups. getResidueEnergies.py can be used from linux terminal. 

### Requirements

**Operating system:** 

Linux

**Software:**

 VMD and NAMD should installed and availabe for call from a terminal window. 

**Python packages: **

ProDy is required for handling of PDB/DCD files and amino acid selections.

Other python packages:

argparse, re, multiprocessing, os, numpy, datetime, glob, natsort, subprocess, mkdir_p, sys, pandas and pickle

### Installation

No need to install. Just place the files into whatever directory you like, allow them to be executable and use by calling them from a linux terminal.

You can also add the folder which you placed the files into your PATH environmental variable if you need to call the script from various different folders.

### Usage Examples

### Credits

### Author