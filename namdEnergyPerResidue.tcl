# A script to evaluate residue-based energies from a trajectory sequentially.
# It uses namdenergy.tcl for this purpose

# Parse arguments
puts "Running"

# psf and dcd file names are by default the first two arguments.
set psfFile [lindex $argv 0]
set dcdFile [lindex $argv 1]

# if a skip is specified, this will be the third argument.
if {[llength $argv] > 2} { set skip [lindex $argv 2] } else { set skip 1}

# if a iterCutoff is specified, this will be the fourth argument.
if {[llength $argv] > 3} { set iterCutoff [lindex $argv 3] } else { set iterCutoff "none"}

#### THIS IS USELESS BUT ALSO HARMLESS NOW.... ###
# if an output folder is specified, this will be the fifth argument.
if {[llength $argv] > 4} { set outputFolder [lindex $argv 4] } else { set outputFolder [pwd]}
#################################################

# if an output string is provided, that is by default the sixth argument
if {[llength $argv] > 5} { set outputString [lindex $argv 5] } else { set outputString "namdenergy"}
puts "outputString is: $outputString"

# if a range for first and last residues, for which energies should be computed, 
# are provided, this is by default the 7th and 8th arguments.
if {[llength $argv] > 6} { set firstLastResidues [lrange $argv 6 7] } else { set firstLastResidues "all"}

# if an iterative calculation is requested, this is by default the 9th argument.
if {[llength $argv] > 8} { set iterCalc [lindex $argv 8] } else { set iterCalc "False"}

# if a range for first and last target residues, which will be the second selection for 
# namdEnergy call, is provided, these are by default the 10th and 11th arguments.
if {[llength $argv] > 9} { set targetFirstLastResidues [lrange $argv 9 10] } else { set targetFirstLastResidues "all"}

# Prepare
package require namdenergy

# Add the DCD file and wait until all frames are loaded.
mol new $psfFile
mol addfile $dcdFile waitfor all step $skip

# If "all" is first and last residues (source), construct the array accordingly.

if { $firstLastResidues == "all"} { 
	set residues [[atomselect top "protein and alpha"] get residue] 
	} else {
		set firstResidue [lindex $firstLastResidues 0]
		set lastResidue [lindex $firstLastResidues 1]
		set residues [[atomselect top "protein and alpha"] get residue]
		set residues [lrange $residues $firstResidue $lastResidue]
	}

# If "all" is first and last residues (target), construct the array accordingly.
if { $targetFirstLastResidues == "all"} { 
	set targetResidues [[atomselect top "protein and alpha"] get residue] 
	} else {
		set targetFirstResidue [lindex $targetFirstLastResidues 0]
		set targetLastResidue [lindex $targetFirstLastResidues 1]
		set targetResidues [[atomselect top "protein and alpha"] get residue]
		set targetResidues [lrange $targetResidues $targetFirstResidue $targetLastResidue]
	}

foreach rd $residues {
		
	set selEnergy [atomselect top "residue $rd"]
	set selEnergyCA [atomselect top "residue $rd and alpha"]
	set selSegname [$selEnergyCA get segname]
	set selResid [$selEnergyCA get resid]
	set selResidue [$selEnergyCA get residue]

 # if iterCalc is False, then calculate energy between individual source residues and all target residues.

	if { $iterCalc == "False" } {
		if { $targetFirstLastResidues == "all" } {
			set oFileString $outputString\_$selResidue\_targetSel_energies.dat
			puts "Calculating all interaction energies for $selResidue ..."
			namdenergy -all -sel $selEnergy -ofile $oFileString -tempname $rd
			} else {
				
				set targetResidueSel [atomselect top "residue $targetResidues"]
				
				# Uncomment the following three lines if you want to have output files named according to segname and resid.
				#set oFileString $outputString\_$selSegname\_$selResid\_energies.dat
				#puts "Calculation non-bonded interaction energy between $selSegname $selResid and targetSel ..."
				#namdenergy -nonb -sel $selEnergy $targetResidueSel -ofile $oFileString -tempname $rd -skip $skip
				
				# Uncomment the following three lines if you want to have output files named according to residue number
				set oFileString $outputString\_$selResidue\_energies.dat
				puts "Calculation non-bonded interaction energy between $selResidue and targetSel ..."
				namdenergy -nonb -sel $selEnergy $targetResidueSel -ofile $oFileString -tempname $rd
			}
	} else { 
		# If iterCalc is True, then calculate energy between individual source residues and individual target residues.
		foreach trd $targetResidues {

			if { $rd != $trd } {

				set selEnergy2 [atomselect top "residue $trd"]
				set selEnergy2CA [atomselect top "residue $trd and alpha"]
				set sel2Segname [$selEnergy2CA get segname]
				set sel2Resid [$selEnergy2CA get resid]
				set sel2Residue [$selEnergy2CA get residue]

				if { $iterCutoff != "none" } {
				
					# Calculate the minimum distance between the two CA. If it is lower than iterCutoff, then do calculation.
					set selEnergyCAindex [$selEnergyCA get index]
					set selEnergy2CAindex [$selEnergy2CA get index]

					label add Bonds 0/$selEnergyCAindex 0/$selEnergy2CAindex
					set distances [label graph Bonds 0]
					set distances_list [lrange $distances 0 end]
					set minimum [lindex [lsort -increasing $distances_list] 0]

					if { $minimum < $iterCutoff } {

						# Uncomment the following three lines if you want to have output files named according to segname and resid.
						#set oFileString $outputString\_$selSegname\_$selResid\_$sel2Segname\_$sel2Resid\_energies.dat
						#puts "Calculating the non-bonded interaction energy between $selSegname $selResid and $sel2Segname $sel2Resid ..."
						#namdenergy -nonb -sel $selEnergy $selEnergy2 -ofile $oFileString -tempname $rd\_$trd -skip $skip

						# Uncomment the following three lines if you want to have output files named according to residue.
						set oFileString $outputString\_$selResidue\_$sel2Residue\_energies.dat
						puts "Calculating the non-bonded interaction energy between $selResidue and $sel2Residue ..."
						namdenergy -nonb -sel $selEnergy $selEnergy2 -ofile $oFileString -tempname $rd\_$trd

					}
					
					# This following line is critical.
					label delete Bonds

				} else {

					# Uncomment the following three lines if you want to have output files named according to segname and resid.
					#set oFileString $outputString\_$selSegname\_$selResid\_$sel2Segname\_$sel2Resid\_energies.dat
					#puts "Calculating the non-bonded interaction energy between $selSegname $selResid and $sel2Segname $sel2Resid ..."
					#namdenergy -nonb -sel $selEnergy $selEnergy2 -ofile $oFileString -tempname $rd\_$trd -skip $skip

					# Uncomment the following three lines if you want to have output files named according to residue.
					set oFileString $outputString\_$selResidue\_$sel2Residue\_energies.dat
					puts "Calculating the non-bonded interaction energy between $selResidue and $sel2Residue..."
					namdenergy -nonb -sel $selEnergy $selEnergy2 -ofile $oFileString -tempname $rd\_$trd

				}
			
			}
		}
	} 
} 

#eval runNamdEnergyPerResidue $argv
exit
