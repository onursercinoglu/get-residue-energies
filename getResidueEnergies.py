#!/usr/bin/env python

# IMPORTS

import argparse
from prody import *
import re
import multiprocessing
import os
import numpy as np
import datetime
import glob
from natsort import natsorted
import subprocess
import mkdir_p
import sys
import pandas
import pickle

# INPUT PARSING
parser = argparse.ArgumentParser(description='Calculate total energy of individual residues \
	of a protein or calculate pairwise nonbonded interaction energies between residues of two \
	selected groups of atoms of a protein over the course of a molecular dynamics DCD trajectory.\
	\n\n\
	getResidueEnergies requires VMD and NAMD to be installed and availabe for call from a \
	terminal window. For calculation of energies, it employs the namdenergy.tcl script from VMD/NAMD \
	developers.')

parser.add_argument('pdb',type=str,nargs=1,help='Name of the corresponding pdb file of the \
	DCD trajectory')

parser.add_argument('psf',type=str,nargs=1,help='Name of the corresponding psf file of the \
	DCD trajectory')

parser.add_argument('dcd',type=str,nargs=1,help='Name of the trajectory DCD file')

parser.add_argument('--numcores',default=[1],type=int,nargs=1,
	help='Number of CPU cores to be employed for energy calculation. If not specified, it \
	defaults to 1 (no parallel computation will be done). If specified, e.g. NUMCORES=n, \
	then the computational workloading will be distributed among n cores.')

parser.add_argument('--sourcesel',default=['all'],nargs='+',help='A ProDy atom selection \
 string which determines the first group of selected residues. ')

parser.add_argument('--targetsel',default=False,type=str,nargs='+',help='A ProDy atom selection string \
	which determines the second group of selected residues.')

parser.add_argument('--paircalc',action='store_true',help='When given, this argument enables pairwise \
	nonbonded interaction energy calculation between sourcesel and targetsel residues. When not given, \
	total energy of each residue in sourcesel will be calculated.')

parser.add_argument('--paircutoff',type=int,default=[15],nargs=1,help='Cutoff distance (angstroms) \
	for pairwise interaction energy calculations. If not specified, it defaults to 15. \
	Only those residues that are within the PAIRCUTOFF distance of each other will be taken \
	into account in energy calculations.')

parser.add_argument('--skip',default=[1],type=int,nargs=1,help='If specified, namdenergy.tcl \
	will use this skip parameter, which defines the number of frames in dcd to skip between \
	each calculation.')

parser.add_argument('--outfolder',default=os.getcwd(),type=str,nargs=1,help='Folder path for storing \
	calculation results. If not specified, the current working folder will be used.')

parser.add_argument('--topickle',default=False,action='store_true',help='When given, the energy values \
	are stored into a pickle file in the current working directory for later import and analysis in python.')

args = parser.parse_args()

# METHODS

def getResidueEnergies(psf,pdb,dcd,numCores=1,sourceSel='protein',targetSel=False,iterCalc=False,
	skip=1,iterCutoff=20,outputFolder=os.getcwd(),toDat=True,compactDat=True,toPickle=False):

	# ARGUMENT CHECKS
	if targetSel == False and iterCalc == True:
		print "ERROR: You must also specify a --targetsel for --paircalc to be executed, or vice versa"
		return

	# Before we start the calculation, check whether the user has specified and outputFolder.
	# If yes, check whether it exists. If it exists, change to that directory and do calculations.
	# If it does not exist, create that directory and change to that directory and do calculations.
	currentFolder = os.getcwd()
	if outputFolder != currentFolder:
		if os.path.isdir(outputFolder):
			os.chdir(outputFolder)
		else:
			mkdir_p.mkdir_p(outputFolder)
			os.chdir(outputFolder)

	psfFilePathNoExt, psfFilePathExt = os.path.splitext(psf)
	psfFileName = os.path.basename(psf)
	psfFileNameNoExt,_ = os.path.splitext(psfFileName)

	# Get the file path and name for DCD without extension
	dcdFilePathNoExt, dcdFilePathExt = os.path.splitext(dcd)
	dcdFileName = os.path.basename(dcd)
	dcdFileNameNoExt,_ = os.path.splitext(dcdFileName)
	
	# Load pdb with prody and get some useful numbers.
	system = parsePDB(pdb)
	source = system.select(sourceSel)
	sourceCA = source.select('calpha')
	numSource = len(sourceCA)
	sourceResids = sourceCA.getResindices()
	sourceResnums = sourceCA.getResnums()
	sourceSegnames = sourceCA.getSegnames()

	allResidues = system.select('all')
	allResiduesCA = allResidues.select('calpha')
	numResidues = len(allResiduesCA)
	numTarget = numResidues

	# By default, targetResids are all residues.
	targetResids = np.arange(numResidues)
	
	# Get target selection residues, if provided:
	if targetSel:
		target = system.select(targetSel)
		targetCA = target.select('calpha')
		numTarget = len(targetCA)
		targetResids = targetCA.getResindices()

	# Start calculation.
	if numCores == 1: # Then the user does not want multiple processors (too bad lol:))
	
		residChunk = [sourceResids[0],sourceResids[-1]]

		calcResidueEnergiesSingleCore(residChunk,psfFilePathNoExt,dcdFilePathNoExt,dcdFileNameNoExt,
			targetSel,targetResids,iterCalc,skip,iterCutoff,outputFolder)

	elif numCores > 1: 
		
		# Start a processor pool.
		pool = multiprocessing.Pool(numCores)

		# Create appropriate chunks of resids according to numCores and the size of sourceSel and 
		if numSource > numTarget or numSource == numTarget:
			residChunks = np.array_split(sourceResids,numCores)

		# Call the method getResidueEnergiesSingleCore multiple times with multiprocessing module.
			for residChunk in residChunks:
				pool.apply_async(calcResidueEnergiesSingleCore,args=(residChunk,psfFilePathNoExt,
					dcdFilePathNoExt,dcdFileNameNoExt,
					targetSel,targetResids,iterCalc,skip,iterCutoff,outputFolder))

		elif numTarget > numSource:
			residChunks = np.array_split(targetResids,numCores)

		# Call the method getResidueEnergiesSingleCore multiple times with multiprocessing module.
			for residChunk in residChunks:
				pool.apply_async(calcResidueEnergiesSingleCore,args=(sourceResids,psfFilePathNoExt,
					dcdFilePathNoExt,dcdFileNameNoExt,
					targetSel,residChunk,iterCalc,skip,iterCutoff,outputFolder))

		pool.close()
		pool.join()

	# Write to files.
	write2files(traj_tuple,numCores,sourceSel,targetSel,targetResids,iterCalc,iterCutoff,
		skip,dcdFilePathNoExt,dcdFileNameNoExt,toPickle,currentFolder,outputFolder)

def write2files(traj_tuple,numCores,sourceSel,targetSel,targetResids,iterCalc,iterCutoff,
	skip,dcdFilePathNoExt,dcdFileNameNoExt,toPickle,currentFolder,outputFolder):

	# Prepare to write: get the date:
	now = datetime.datetime.now()

	# Write into files. WARNING: Long if/elif/for stacks. I know it is not optimal.
	if iterCalc == False:

		# Parse the .dat files generated by namdenergy into a list.
		energyOutputFiles = parseEnergyOutput('%s_[^_]+_energies.dat' % dcdFileNameNoExt)
		
		# Get file names.
		fileKeys = energyOutputFiles.keys()
		fileKeys = natsorted(fileKeys)
		numKeys = len(fileKeys)
		
		# Get energy types from the first file.
		energyTypes = energyOutputFiles[fileKeys[0]].keys()

		# If the results are also requested to be saved in pickle files, then start a dict for storing it later.
		if toPickle == True:
			energiesArray = dict()

		# Write into separate files according to energy types.
		for i in range(0,len(energyTypes)):

			# Get the curent energy type
			energyType = energyTypes[i]

			# If energyType is Time or Frame, then it is not an energy type. skip to next 
			# iteration.
			if energyType == 'Time' or energyType == 'Frame':
				continue

			if toPickle == True:
				energiesArray[energyType] = dict()
			
			# Print a header information.
			fileName = '%s_sourceSel_%s.dat' % (dcdFileNameNoExt,energyType)
			print ('Writing energies to file %s...' % fileName)
			f = open(fileName,'w')
			f.write('# getResidueEnergies.py was run on: \n')
			f.write('# \n')
			f.write('# Date: %d.%d.%d %d:%d \n' % (now.year,now.month,now.day,now.hour,now.minute))
			f.write('# \n')
			f.write('# with the following options:\n')
			f.write('# \n')
			f.write('# --pdb\t\t\t%s\n' % traj_tuple[0])
			f.write('# --dcd\t\t\t%s\n' % traj_tuple[1])
			f.write('# --numcores\t\t\t%s\n' % numCores)
			f.write('# --sourcesel\t\t\t%s\n' % sourceSel)
			f.write('# --targetsel\t\t\t%s\n' % targetSel)
			f.write('# --paircalc\t\t\t%s\n' % iterCalc)
			f.write('# --paircutoff\t\t\t%d\n' % iterCutoff)
			f.write('# --skip\t\t\t%d\n' % skip)
			f.write('# \n')
			f.write('# This file lists %s energies for each residue in sourceSel (%s)\n' % (energyType,sourceSel))
			f.write('# \n')

			# Print data headers.
			numFrames = len(energyOutputFiles[fileKeys[0]][energyType])

			titleString = 'Frame'

			for k in range(0,numKeys):
				# Get the residue segname and resid from file's name and assign it as column header.
				matches = re.match(('%s_([^_]+)_energies.dat' % dcdFileNameNoExt),
					fileKeys[k])
				activeColumnTitle = matches.groups()[0]
				titleString = titleString+'\t'+activeColumnTitle

			f.write(titleString)
			f.write('\n')

			# Print actual data.
			for l in range(0,numFrames):
				lineString = str(l)

				for m in range(0,numKeys):
					lineString = lineString+'\t'+str(energyOutputFiles[fileKeys[m]][energyType][l])

				f.write(lineString)
				f.write('\n')

			f.close()

			if toPickle == True:
				# Read the file back as a pandas DataFrame.
				residueEnergies = pandas.read_table(fileName,skiprows=17,index_col=0)
			
				# Rename column names to have integer column names. Somehow pandas loads integer columns names
				# still as strings. 
				colnames = residueEnergies.columns
				colnames_int = map(int,colnames)
				residueEnergies.columns = colnames_int

				# Append it at the appropriate position of energiesArray.
				energiesArray[energyType] = residueEnergies

		# Clean up files, because compactDat is True.
		for n in range(0,numKeys):
			os.remove(fileKeys[n])

		os.chdir(currentFolder)
			
		if toPickle == True:
			energiesFile = open('%s.pickle' % outputFolder,'w')
			pickle.dump(energiesArray,energiesFile)
			energiesFile.close()

	elif iterCalc == True:

		# Parse the .dat file generated by namdenergy into a list.
		energyOutputFiles = parseEnergyOutput('%s_[^_]+_[^_]+_energies.dat' % 
			dcdFileNameNoExt)

		# Get file names.
		fileKeys = energyOutputFiles.keys()
		fileKeys = natsorted(fileKeys)
		numKeys = len(fileKeys)

		# Get unique source residues.
		uniqueSources = list()
		for i in range(0,numKeys):
			matches = re.match(('%s_([^_]+)_[^_]+_energies.dat' % dcdFileNameNoExt),
				fileKeys[i])
			uniqueSources.append(matches.groups()[0])

		uniqueSources = np.unique(uniqueSources)
		numUniqueSources = len(uniqueSources)

		# If the results are also requested to be saved in pickle files, then start a dict for storing it later.
		if toPickle == True:
			energiesArray = dict()
		
		# Get energy types from the first file.
		energyTypes = energyOutputFiles[fileKeys[0]].keys()

		# Write into separate files according to energy types and sourceSel residues.
		for i in range(0,len(energyTypes)):
			energyType = energyTypes[i]

			if energyType == 'Time' or energyType == 'Frame':
				continue

			if toPickle == True:
				energiesArray[energyType] = dict()

			for j in range(0,numUniqueSources):
				# Get active source residue
				activeSourceResidue = uniqueSources[j]

				# Print a header information.
				fileName = '%s_%s_targetSel_%s.dat' % (dcdFileNameNoExt,activeSourceResidue,energyType)
				print ('Writing energies to file %s...' % fileName)
				f = open(fileName,'w')
				f.write('# getResidueEnergies.py was run on: \n')
				f.write('# \n')
				f.write('# Date: %d.%d.%d %d:%d \n' % (now.year,now.month,now.day,now.hour,now.minute))
				f.write('# \n')
				f.write('# with the following options:\n')
				f.write('# \n')
				f.write('# --pdb\t\t\t%s\n' % traj_tuple[0])
				f.write('# --dcd\t\t\t%s\n' % traj_tuple[1])
				f.write('# --numcores\t\t\t%s\n' % numCores)
				f.write('# --sourcesel\t\t\t%s\n' % sourceSel)
				f.write('# --targetsel\t\t\t%s\n' % targetSel)
				f.write('# --paircalc\t\t\t%s\n' % iterCalc)
				f.write('# --paircutoff\t\t\t%d\n' % iterCutoff)
				f.write('# --skip\t\t\t%d\n' % skip)
				f.write('# \n')
				f.write('# This file lists %s energies between sourcesel (%s) residue %s and targetsel (%s) residues\n' % (energyType,sourceSel,activeSourceResidue,targetSel))
				f.write('# \n')

				numFrames = len(energyOutputFiles[fileKeys[0]][energyType])

				# Find the files with the active source residues.
				activeSourceTargetResidues = list()
				for k in range(0,numKeys):
					matches = re.match(('%s_%s_([^_]+)_energies.dat' % (dcdFileNameNoExt,
						activeSourceResidue)),fileKeys[k])

					# If not match is found, continue with the next iterations of this for loop.
					if not matches:
						continue
					else:
						activeSourceTargetResidues.append(matches.groups()[0])

				numActiveSourceTargetResidues = len(activeSourceTargetResidues)

				# Construct the title string including all target residues, regardless of paircutoff.
				titleString = 'Frame'
				for k in range(0,len(targetResids)):
					activeTargetResid = targetResids[k]
					titleString = titleString+'\t'+str(activeTargetResid)

				f.write(titleString)
				f.write('\n')

				# Write pairwise interaction energy values line-by-line according to numFrames
				for l in range(0,numFrames):
					lineString = str(l)

					# For each targetResid, write the interaction energy at that frame if the energy for that
					# pair is calculated. If not calculated, write a zero (no interaction energy)
					for m in range(0,len(targetResids)):
						if str(targetResids[m]) in activeSourceTargetResidues:
							fileKey = ('%s_%s_%s_energies.dat' % (dcdFileNameNoExt,
								activeSourceResidue,str(targetResids[m])))
							lineString = lineString+'\t'+str(energyOutputFiles[fileKey][energyType][l])
						else:
							lineString = lineString+'\t'+str(0)

					f.write(lineString)
					f.write('\n')

				f.close()

				if toPickle == True:
					# Read the file back as a pandas DataFrame.
					activeSourceResidueEnergies = pandas.read_table(fileName,skiprows=17,index_col=0)
					
					# Rename column names to have integer column names. Somehow pandas loads integer columns names
					# still as strings. 
					colnames = activeSourceResidueEnergies.columns
					colnames_int = map(int,colnames)
					activeSourceResidueEnergies.columns = colnames_int

					# Append it at the appropriate position of energiesArray.
					energiesArray[energyType][int(activeSourceResidue)] = activeSourceResidueEnergies

		# Clean up files, because compactDat is True.
		for n in range(0,numKeys):
			os.remove(fileKeys[n])

		os.chdir(currentFolder)
			
		if toPickle == True:
			energiesFile = open('%s.pickle' % outputFolder,'w')
			pickle.dump(energiesArray,energiesFile)
			energiesFile.close()

	# # Pack the energy outputs into a list.
	# energyOutputResidues = list()

	# return

	# for i in range(0,numResidues):
	# 	residueKey = '%s_%s_%i_energies.dat' % (dcdFilePathNoExt,segnames[i],resnums[i])
	# 	residueOutput = energyOutputFiles[residueKey]
	# 	energyOutputResidues.append(residueOutput)

	# 	# Delete the namdenergy output dat file if necessary.
	# 	if not toFile == True and compactFile == False:
	# 		os.remove(residueKey)

	# if toFile == True and compactFile == True: # Create a compact results file.
	# 	writeEnergies2file(energyOutputResidues,dcdFilePathNoExt,fileType='dat')

	# if toPickle == True:
	# 	writeEnergies2file(energyOutputResidues,dcdFilePathNoExt,fileType='pickle')

	# return energyOutputResidues

def calcResidueEnergiesSingleCore(residChunk,psfFilePathNoExt,dcdFilePathNoExt,dcdFileNameNoExt,
	targetSel,targetResids,iterCalc,skip,iterCutoff,outputFolder):
	"""
	Do not call this method directly.
	"""

	# Get the path of module, necessary when providing vmd the location of namdEnergyPerResidues.tcl.
	# (see below).
	module_path = sys.path[0]

	# Start a devnull for storing vmd/namd output.
	devnull = open(os.devnull,'w')

	# If targetSel and iterCalc are False:
	# calculate the total energy between individial residues of sourceSel and all the rest of the protein.
	# this is equivalent to calculating the total energy of each residue in sourceSel.
	if targetSel == False and iterCalc == False:
		subprocess.call('vmd -dispdev text -e %s/namdEnergyPerResidue.tcl -args %s.psf %s.dcd %i %i %s %s %i %i' % \
			(module_path,psfFilePathNoExt,dcdFilePathNoExt,skip,iterCutoff,outputFolder,dcdFileNameNoExt,residChunk[0],
				residChunk[-1]),shell=True,stdout=devnull)

	# If targetSel is provided (not False):
	# iterCalc is provided to namdEnergyPerResidue.
	# If iterCalc is False: calculate the non-bonded energy between individiual residues of sourceSel and all the targetSel residues.
	# If iterCalc is True: calculate the non-bonded energy between individiual residues of sourceSel and individiual residues of targetSel residues.
	elif targetSel != False:
		subprocess.call('vmd -dispdev text -e %s/namdEnergyPerResidue.tcl -args %s.psf %s.dcd %i %i %s %s %i %i %s %i %i' % \
			(module_path,psfFilePathNoExt,dcdFilePathNoExt,skip,iterCutoff,outputFolder,dcdFileNameNoExt,residChunk[0],
				residChunk[-1],iterCalc,targetResids[0],targetResids[-1]),shell=True,stdout=devnull)

def parseEnergyOutput(regExpEnergyOutputFiles):
	"""
	parses an output file of namdenergy.tcl or a regular expression which denotes a group of namdenergy.tcl output files, then returns the contents in a dict.

	Parameters
	----------
	regExpEnergyOutputFiles: string, required
		denotes an output file string or a regular expression which denotes a group of output files.

	Returns
	-------
	energyOutputs: dict
		a dictionary with file names as keys and dictionaries with appropriate keys from output files as values.

	Author
	------
	Onur Sercinoglu
	"""

	# Real all output files matching the regular expression given in regExpEnergyOutputFiles.
	fileNames = glob.glob('*')
	energyOutputFileNames = list()
	for i in range(0,len(fileNames)):
		matches = re.match(regExpEnergyOutputFiles,fileNames[i])
		if matches:
			energyOutputFileNames.append(matches.group())

	energyOutputFileNames.sort()

	# Start a dict for storing outputs.
	energyOutputs = dict()

	# Read in the files one by one
	for fileName in energyOutputFileNames:
		
		# Start a dict for storing output.
		energyOutput = dict()

		# Read in the first line (header) output file and count number of total lines.
		f = open(fileName,'r')
		lines = f.readlines()
		numLines = len(lines)
		header = lines[0].split()
		numTitles = len(header)

		# Close the file and reopen it with numpy's loadtxt, which is much more practical for numeric data.
		f.close()
		energies = np.loadtxt(fileName,skiprows=1) # Skipping the header row.

		# Assign each column into appropriate key's value in energyOutput dict.
		for i in range(0,numTitles):
			energyOutput[header[i]] = energies[:,i]

		# Append energyOutput into appropriate key's value in energOutputs dict.
		energyOutputs[fileName] = energyOutput

	return energyOutputs

# CALLS

if len(args.sourcesel) > 1:
	sourceSel = ' '.join(args.sourcesel)
else:
	sourceSel = args.sourcesel[0]

if args.targetsel:
	targetSel = ' '.join(args.targetsel)
else:
	targetSel = False

getResidueEnergies(psf=args.psf[0],pdb=args.pdb[0],dcd=args.dcd[0],
	numCores=args.numcores[0],sourceSel=sourceSel,targetSel=targetSel,iterCalc=args.paircalc,
	skip=args.skip[0],iterCutoff=args.paircutoff[0],outputFolder=args.outfolder[0],toPickle=args.topickle)
